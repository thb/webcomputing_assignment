# Sample eegister/login page with python cgi

## URL
http://kronos.th-brandenburg.de/~beilich/cgi-bin/index.cgi

## Additional information
- the css is built from scss
- the python cgi server (which was used for local development) does not understand redirects via status codes, so a workaround is used
- the source can be found at https://gitlab.beilich.de/thb/webcomputing_assignment
