#!/usr/bin/python
import http.cookies as Cookie
from os import environ

cookie = Cookie.SimpleCookie(environ['HTTP_COOKIE'])
for key, value in cookie.items():
    cookie[key] = ''
    cookie[key]['expires']='Thu, 01 Jan 1970 00:00:00 GMT'
print(cookie.output())
print('Content-type: text/html\n')
print('<html><head><meta http-equiv="refresh" content="0;url=index.cgi" /></head></html>')
