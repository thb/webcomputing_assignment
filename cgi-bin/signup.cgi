#!/usr/bin/python
import utilities
import cgi
import http.cookies as Cookie
from os import environ

if environ['REQUEST_METHOD'] == 'GET':
    result = utilities.getContentWithLayout('../src/html/layout.html', '../src/html/signup.html')
    result = result.replace('{{title}}','Signup')
    result = result.replace('{{signup_active}}','active')
    print(result)
if environ['REQUEST_METHOD'] == 'POST':
    resultPage = 'signup.cgi'

    form = cgi.FieldStorage()
    name = form.getfirst('username', '')
    password = form.getfirst('password', '')
    password_repeat = form.getfirst('password_repeat', '')

    if password == password_repeat:
        resultPage = 'index.cgi'
        cookie = Cookie.SimpleCookie()
        registeredUsers = ''
        try:
            cookie = Cookie.SimpleCookie(environ['HTTP_COOKIE'])
            registeredUsers = cookie['registeredUsers'].value
        except (Cookie.CookieError, KeyError):
            pass
        cookie['registeredUsers'] = '%s%s:%s;' % (registeredUsers, name, password)
        print(cookie.output())

    print('Content-type: text/html\n')
    print('<html><head><meta http-equiv="refresh" content="0;url=%s" /></head></html>' % resultPage)
