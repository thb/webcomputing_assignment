#!/usr/bin/python
import re
import http.cookies as Cookie
from os import environ

def __includeTemplates(site):
    templates = set(re.findall(r'{file% .* %}', site))
    result = site
    for template in templates:
        filePath = template[6:-2].strip()
        with open(filePath, 'r') as file:
            data = file.read()
            result = result.replace(template, data)
    return result

def getContentWithLayout(layoutFilepath, contentFilepath):
    result = 'Content-type:text/html\n\n'

    with open(layoutFilepath, 'r') as layoutFile:
        layout = layoutFile.read()
    with open(contentFilepath, 'r') as contentFile:
        content = contentFile.read()

    result += layout.replace('{% content %}', content)
    result = __includeTemplates(result)
    return result

def isValidUser(username, password):
    try:
        cookie = Cookie.SimpleCookie(environ['HTTP_COOKIE'])
        registeredUsers = cookie['registeredUsers'].value.split(';')
        registeredUsers = filter(None, registeredUsers)
        for user in registeredUsers:
            (name, pw) = user.split(':')
            if name == username and pw == password:
                return True
    except (Cookie.CookieError, KeyError):
        pass
    return False
