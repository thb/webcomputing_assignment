#!/usr/bin/python
import utilities
import http.cookies as Cookie
import cgi
from os import environ

title = 'Login'
result = utilities.getContentWithLayout('../src/html/layout.html', '../src/html/login.html')

try:
    cookie = Cookie.SimpleCookie(environ['HTTP_COOKIE'])
    username = cookie['username'].value
    password = cookie['password'].value
    if username == 'admin':
        result = utilities.getContentWithLayout('../src/html/layout.html', '../src/html/admin.html')
        title = 'Secret Admin Area'
    if utilities.isValidUser(username, password):
        result = utilities.getContentWithLayout('../src/html/layout.html', '../src/html/secret.html')
        title = 'Main Page'
        result = result.replace('{{username}}',username)
    result = result.replace('{{title}}',title)
    print(result)
except (Cookie.CookieError, KeyError):
    print('Content-type: text/html\n')
    print('<html><head><meta http-equiv="refresh" content="0;url=login.cgi" /></head></html>')
