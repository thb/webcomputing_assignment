#!/usr/bin/python
import utilities
import http.cookies as Cookie
import cgi
from os import environ

if environ['REQUEST_METHOD'] == 'GET':
    result = utilities.getContentWithLayout('../src/html/layout.html', '../src/html/login.html')
    result = result.replace('{{title}}','Login')
    result = result.replace('{{login_active}}','active')
    print(result)
if environ['REQUEST_METHOD'] == 'POST':
    form = cgi.FieldStorage()
    username = form.getfirst('username', '')
    password = form.getfirst('password', '')

    if (username == 'admin' and password == 'secret') or utilities.isValidUser(username, password):
        cookie = Cookie.SimpleCookie()
        cookie['username'] = username
        cookie['password'] = password
        print(cookie.output())

    print('Content-type: text/html\n')
    print('<html><head><meta http-equiv="refresh" content="0;url=index.cgi" /></head></html>')
